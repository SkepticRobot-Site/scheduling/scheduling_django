# Generated by Django 3.2.3 on 2021-06-21 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_rename_name_choice_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='creator',
            field=models.CharField(default='unknown', max_length=255),
        ),
    ]
