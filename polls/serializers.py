from rest_framework import serializers

from .models import ChoiceBoardGame, Poll,Choice

class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = (
            "id",
            "name",
            "type",
            "local_users",
            "choices",
        )
        extra_kwargs = {'choices': {'required': False}}

class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = (
            "id",
            "poll",
            "title",
            "score",
            "votes",
            "creator",
        )

class ChoiceBoardGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChoiceBoardGame
        fields = (
            "id",
            "poll",
            "title",
            "score",
            "votes",
            "bgg_id",
            "weight",
            "rating",
            "play_time",
            "min_players",
            "max_players",
            "best_players",
            "creator",
        )