from django.db import models

# Create your models here.
class Poll(models.Model):
    """docstring for ."""
    NORMAL = 'N'
    BOARDGAME = 'BG'
    TYPE_CHOICES= [
        (NORMAL,'Normal'),
        (BOARDGAME,'Boardgame'),
    ]
    name = models.CharField(max_length=255)
    type = models.CharField(
        max_length=2,
        choices=TYPE_CHOICES,
        default=NORMAL,
    )
    local_users = models.JSONField(default = dict)

    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering =('-date_added',)

    def __str__(self):
        return self.name

class Choice(models.Model):
    poll = models.ForeignKey(Poll, related_name='choices', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    creator = models.CharField(max_length=255,default='unknown')

    score = models.IntegerField(default = 0)
    votes = models.JSONField(default = dict,blank = True)

    date_added = models.DateTimeField(auto_now_add=True)

class ChoiceBoardGame(Choice):
    bgg_id = models.IntegerField()
    rating = models.IntegerField()
    weight = models.IntegerField()
    play_time = models.IntegerField()
    min_players = models.IntegerField()
    max_players = models.IntegerField()
    best_players = models.IntegerField()
