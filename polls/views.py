import logging 

from django.http import Http404
from django.db.models import F

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets

# Create your views here.
from .models import Poll, Choice, ChoiceBoardGame
from .serializers import ChoiceBoardGameSerializer, PollSerializer, ChoiceSerializer

logger =logging.getLogger(__name__)



class PollViewSet(viewsets.ModelViewSet):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer

    @action(detail=True,methods=['patch'])
    def add_local_user(self,request,pk=None):
        try:
            poll=self.get_object()
            username = request.data['username']
            password = request.data['password']
            poll.local_users[username] = password
            poll.save(update_fields=['local_users',])
        except Exception as error:
            logger.debug(error)
            return Response(error,status=400)
        return Response(status=200)

class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer

    @action(detail=True,methods=['patch'])
    def vote(self,request,pk=None):
        try:
            choice = self.get_object()
            username = request.data['username']
            vote = request.data['vote']
            if('old_vote' in request.data):
                old_vote =  request.data['old_vote']
                if(vote == old_vote):
                    del choice.votes[username]
                    choice.score = F('score') - vote
                else:
                    choice.votes[username] = vote
                    choice.score = F('score') + vote - old_vote
            else:
                choice.votes[username] = vote
                choice.score = F('score') + vote
            choice.save(update_fields=['votes','score'])
        except Exception as error:
            logger.debug(error)
            return Response(error,status=400)
        return Response(status=200)

class ChoiceBoardGameViewSet(ChoiceViewSet):
    queryset=ChoiceBoardGame.objects.all()
    serializer_class = ChoiceBoardGameSerializer