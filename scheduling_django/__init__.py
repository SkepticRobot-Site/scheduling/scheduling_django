import logging
import logging.handlers

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

rh = logging.handlers.RotatingFileHandler("scheduling_django.log",maxBytes=1000000,backupCount=2)
rh.setLevel(logging.DEBUG)

format =logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

rh.setFormatter(format)

logger.addHandler(rh)